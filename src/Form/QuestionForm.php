<?php

namespace Drupal\viko_ai\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\viko_ai\VikoAiClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Viko.ai Question' block form.
 */
class QuestionForm extends FormBase {

  /**
   * The Viko.ai API client.
   *
   * @var \Drupal\viko_ai\VikoAiClientInterface
   */
  protected $client;

  /**
   * Constructs a new QuestionForm instance.
   *
   * @param \Drupal\viko_ai\VikoAiClientInterface $viko_ai_client
   *   The Viko.ai API client.
   */
  public function __construct(VikoAiClientInterface $viko_ai_client) {
    $this->client = $viko_ai_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('viko_ai.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'viko_ai_question_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->prepareClient($form_state);

    // Attach AJAX behavior to whole form.
    $ajax = [
      'callback' => '::ajaxCallback',
      'wrapper' => 'viko-ai-question-form-wrapper',
      'effect' => 'fade',
    ];
    $form['#prefix'] = '<div id="viko-ai-question-form-wrapper">';
    $form['#suffix'] = '</div>';

    // Get current step information.
    $step = $form_state->get('viko_ai_step') ?? 'question';
    $question = $form_state->get('viko_ai_question');
    $answer = $form_state->get('viko_ai_answer');

    if (!\Drupal::service('path.matcher')->isFrontPage()) {
      // Get folder ID based on URL first segment.
      $folder = viko_ai_get_folder();
    }

    if ($step === 'question') {
      $form['text'] = [
        '#markup' => $this->t('Find answers to common questions or submit your own.'),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];
      $form['question'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Submit your question'),
        '#default_value' => $question,
        '#required' => TRUE,
      ];
      $form['folder'] = [
        '#type' => 'value',
        '#value' => $folder ?? '',
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Ask question'),
        '#ajax' => $ajax,
      ];
    }

    elseif ($step === 'answer') {
      $form['text'] = [
        '#markup' => $this->t('You asked "@question"', [
          '@question' => $question,
        ]),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];
      $link = '';
      // Wrap title with link if available.
      if (!empty($answer->link)) {
        $link = ' <a href="' . $answer->link . '">'
          . $this->t('read more') . '</a>';
      }
      if (!empty($answer->answer)) {
        $form['answer'] = [
          '#markup' => $answer->answer,
          '#prefix' => '<p><b>',
          '#suffix' => '</b>' . (empty($answer->context) ? $link : '') . '</p>',
        ];
      }
      if (!empty($answer->context)) {
        $form['context'] = [
          '#markup' => $answer->context . $link,
          '#prefix' => '<p>',
          '#suffix' => '</p>',
        ];
      }
      $form['submit'] = [
        '#markup' => $this->t('Did this answer your question?') . '<br/>',
      ];
      $form['submit']['yes'] = [
        '#type' => 'submit',
        '#name' => 'viko_ai_button_yes',
        '#value' => $this->t('yes'),
        '#ajax' => $ajax,
      ];
      $form['submit']['no'] = [
        '#type' => 'submit',
        '#name' => 'viko_ai_button_no',
        '#value' => $this->t('no'),
        '#ajax' => $ajax,
      ];
    }

    elseif ($step === 'thanks') {
      $form['text'] = [
        '#markup' => $this->t('Thank you for your answer!'),
        '#prefix' => '<p><b>',
        '#suffix' => '</b></p>',
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Ask again'),
        '#ajax' => $ajax,
      ];
    }

    elseif ($step === 'forward') {
      $form['text_1'] = [
        '#markup' => $this->t('You asked "@question"', [
          '@question' => $question,
        ]),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];
      $form['text_2'] = [
        '#markup' => $this->t("Sorry, we can't find an answer here. We'll forward your question to a member of the team."),
        '#prefix' => '<p>',
        '#suffix' => '</b>',
      ];
      $form['text_3'] = [
        '#markup' => $this->t('Please provide your email address, you should receive an answer withing the hour.'),
        '#prefix' => '<p>',
        '#suffix' => '</b>',
      ];
      $form['email'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Your email'),
        '#required' => TRUE,
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Forward my question'),
        '#ajax' => $ajax,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->prepareClient($form_state);

    // Get current step information.
    $step = $form_state->get('viko_ai_step') ?? 'question';
    $trigger = $form_state->getTriggeringElement();

    if ($step === 'question') {
      $question = $form_state->getValue('question');
      $form_state->set('viko_ai_question', $question);
      $folder = $form_state->getValue('folder') ?? '';
      $folders = array_filter([$folder]);
      $answer = $this->client->askQuestion($question, $folders);

      if (!empty($answer)) {
        $form_state->set('viko_ai_answer', $answer);
        $form_state->set('viko_ai_step', 'answer');
      }
      else {
        $form_state->set('viko_ai_step', 'forward');
      }
    }

    elseif ($step === 'answer') {
      $answer = $form_state->get('viko_ai_answer');
      if ($trigger['#name'] === 'viko_ai_button_yes') {
        $this->client->answerQuestion($answer->id, TRUE);
        $form_state->set('viko_ai_step', 'thanks');
      }
      elseif ($trigger['#name'] === 'viko_ai_button_no') {
        $this->client->answerQuestion($answer->id, FALSE);
        $form_state->set('viko_ai_step', 'forward');
      }
    }

    elseif ($step === 'thanks') {
      $form_state->set('viko_ai_step', 'question');
    }

    elseif ($step === 'forward') {
      $answer = $form_state->get('viko_ai_answer');
      $email = $form_state->getValue('email');
      $this->client->forwardQuestion($answer->id, $email);
      $form_state->set('viko_ai_step', 'thanks');
    }

    $form_state->setRebuild();
  }

  /**
   * AJAX callback to update the entire form.
   */
  public function ajaxCallback(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Configure API client from block settings.
   */
  private function prepareClient(FormStateInterface $form_state) {
    $config = $form_state->getBuildInfo()['args'][0];
    /** @var \Drupal\search_api\ServerInterface $server */
    $server = \Drupal::entityTypeManager()
      ->getStorage('search_api_server')
      ->load($config['server']);
    /** @var \Drupal\viko_ai\VikoAiClientInterface $client */
    $this->client->fromOptions($server->getBackendConfig()['client']);
  }

}
