<?php

namespace Drupal\viko_ai\Plugin\search_api\backend;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\SearchApiException;
use Drupal\viko_ai\VikoAiClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Indexes items using the Viko.ai API.
 *
 * @SearchApiBackend(
 *   id = "viko_ai",
 *   label = @Translation("Viko.ai"),
 *   description = @Translation("Indexes items using the Viko.ai API.")
 * )
 */
class VikoAi extends BackendPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * The Viko.ai API client.
   *
   * @var \Drupal\viko_ai\VikoAiClientInterface
   */
  protected $client;

  /**
   * Constructs a new VikoAi instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\viko_ai\VikoAiClientInterface $viko_ai_client
   *   The Viko.ai API client.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, VikoAiClientInterface $viko_ai_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Configure API client from provided settings.
    if (!empty($configuration['client'])) {
      $this->client = $viko_ai_client->fromOptions($configuration['client']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('viko_ai.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'client' => [
        'base_uri' => 'https://api.viko.ai/v1-beta/',
        'tenancy' => '',
        'key' => '',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['client'] = [
      '#type' => 'details',
      '#title' => $this->t('Viko.ai client options'),
      '#open' => TRUE,
    ];
    $form['client']['base_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URI'),
      '#description' => t('The base URI for the client connection.'),
      '#default_value' => $this->configuration['client']['base_uri'],
      '#required' => TRUE,
    ];
    $form['client']['tenancy'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API tenancy'),
      '#description' => t('The tenancy for the client connection.'),
      '#default_value' => $this->configuration['client']['tenancy'],
      '#required' => TRUE,
    ];
    $form['client']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => t('The key for the client connection.'),
      '#default_value' => $this->configuration['client']['key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    try {
      // Test API credentials from the current form.
      $config = $form_state->getValue('client');
      $this->client->fromOptions($config)->listDocumentFolders();
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('client', $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings() {
    $info[] = [
      'label' => $this->t('API client base URI'),
      'info' => $this->configuration['client']['base_uri']
        . $this->configuration['client']['tenancy'] . '/',
    ];
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function addIndex(IndexInterface $index) {
    parent::addIndex($index);

    // Create DB storage for index item information.
    $name = 'search_api_viko_ai_' . $index->id();
    if (!\Drupal::database()->schema()->tableExists($name)) {
      $table = [
        'name' => $name,
        'module' => 'viko_ai',
        'fields' => [
          'item_id' => [
            'description' => 'The item ID.',
            'type' => 'varchar',
            'length' => 150,
            'not null' => TRUE,
          ],
          'folder' => [
            'description' => 'The item folder.',
            'type' => 'varchar',
            'length' => 255,
            'not null' => TRUE,
          ],
        ],
      ];
      \Drupal::database()->schema()->createTable($name, $table);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeIndex($index) {
    parent::removeIndex($index);

    // Delete DB storage for index item information.
    $name = 'search_api_viko_ai_' . $index->id();
    \Drupal::database()->schema()->dropTable($name);
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items) {
    $indexed = [];
    $mapping = $this->getFolderMapping($index, array_keys($items));

    foreach ($items as $id => $item) {
      try {
        $this->indexItem($item, $mapping);
        $indexed[] = $id;
      }
      catch (\Exception $e) {
        $this->getLogger()->warning($e->getMessage());
      }
    }
    return $indexed;
  }

  /**
   * Indexes a single item on the specified index.
   *
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The item to index.
   * @param array $mapping
   *   The folder mapping.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   If an indexing error occurs.
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  protected function indexItem(ItemInterface $item, array $mapping) {
    // Convert Search API item ID to Viko.ai item ID.
    $search_api_id = $item->getId();
    $id = str_replace('/', '-', $search_api_id);

    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $item->getOriginalObject()->getValue();
    $title = $entity->label();

    try {
      // Get folder ID based on URL first segment.
      $url = $entity->toUrl('canonical', ['absolute' => TRUE])->toString();
      $folder = viko_ai_get_folder(parse_url($url)['path']);
    }
    finally {
      if (empty($folder)) {
        throw new SearchApiException('Folder is unknown for entity: ' . $id);
      }
    }

    // Prepare item values to be indexed.
    // TODO: Only "Rendered HTML output" is supported.
    $values = $item->getField('rendered_item')->getValues();
    if (!empty($values[0])) {
      $type = 'text/html';
      $body = $values[0]->getText();
    }
    if (empty($body)) {
      throw new SearchApiException('Body text is empty for entity: ' . $id);
    }

    try {
      // Delete item first, if folder was changed.
      if (!empty($mapping[$search_api_id])
        && $folder !== $mapping[$search_api_id]
      ) {
        $this->client->deleteDocument($mapping[$search_api_id], $id);
      }
      // Call API to set the item values.
      $this->client->putDocument($folder, $id, $type, $body, $title, $url);
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage());
    }

    // Store folder mapping.
    $name = 'search_api_viko_ai_' . $item->getIndex()->id();
    \Drupal::database()->merge($name)->keys(['item_id' => $search_api_id])
      ->fields(['folder' => $folder])->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItems(IndexInterface $index, array $item_ids) {
    $mapping = $this->getFolderMapping($index, $item_ids);

    foreach ($item_ids as $item_id) {
      // Convert Search API item ID to Viko.ai item ID.
      $id = str_replace('/', '-', $item_id);

      // Only if item mapping exists.
      if (!empty($mapping[$item_id])) {
        try {
          // Call API to delete the item.
          $this->client->deleteDocument($mapping[$item_id], $id);
        }
        catch (\Exception $e) {
          throw new SearchApiException($e->getMessage());
        }

        // Delete folder mapping.
        $name = 'search_api_viko_ai_' . $index->id();
        \Drupal::database()->delete($name)
          ->condition('item_id', $item_id)->execute();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    try {
      // Call API to clear the whole database.
      $this->client->reset();
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage());
    }

    // Delete folder mapping.
    $name = 'search_api_viko_ai_' . $index->id();
    \Drupal::database()->delete($name)->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
    // Get number of index DB storage items.
    $name = 'search_api_viko_ai_' . $query->getIndex()->id();
    $count = \Drupal::database()->select($name)
      ->countQuery()->execute()->fetchField();
    $query->getResults()->setResultCount($count);
  }

  /**
   * Get folder mapping for item IDs.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The search index.
   * @param array $item_ids
   *   The item IDs.
   *
   * @return array
   *   The folder mapping.
   */
  protected function getFolderMapping(IndexInterface $index, array $item_ids) {
    $name = 'search_api_viko_ai_' . $index->id();
    $query = \Drupal::database()->select($name, 'v');
    $query->fields('v', ['item_id', 'folder']);
    $query->condition('v.item_id', $item_ids, 'IN');
    return $query->execute()->fetchAllKeyed();
  }

}
