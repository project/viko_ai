<?php

namespace Drupal\viko_ai\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\viko_ai\Form\QuestionForm;

/**
 * Provides a 'Viko.ai Question' block.
 *
 * @Block(
 *   id = "viko_ai_question",
 *   admin_label = @Translation("Viko.ai Question"),
 *   category = @Translation("Search")
 * )
 */
class VikoAiQuestion extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'server' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['server'] = [
      '#type' => 'select',
      '#title' => $this->t('Server'),
      '#description' => $this->t('The server this block should use.'),
      '#options' => viko_ai_get_servers(),
      '#default_value' => $this->configuration['server'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['server'] = $form_state->getValue('server');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Render form using block settings.
    return \Drupal::formBuilder()
      ->getForm(QuestionForm::class, $this->configuration);
  }

}
