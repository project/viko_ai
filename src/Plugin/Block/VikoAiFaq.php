<?php

namespace Drupal\viko_ai\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Viko.ai FAQ' block.
 *
 * @Block(
 *   id = "viko_ai_faq",
 *   admin_label = @Translation("Viko.ai FAQ"),
 *   category = @Translation("Search")
 * )
 */
class VikoAiFaq extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'server' => '',
      'default_folder' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['server'] = [
      '#type' => 'select',
      '#title' => $this->t('Server'),
      '#description' => $this->t('The server this block should use.'),
      '#options' => viko_ai_get_servers(),
      '#default_value' => $this->configuration['server'],
      '#required' => TRUE,
    ];
    $form['default_folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default folder'),
      '#description' => $this->t('The folder to use if the first URL segment is empty.'),
      '#default_value' => $this->configuration['default_folder'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['server'] = $form_state->getValue('server');
    $this->configuration['default_folder'] = $form_state->getValue('default_folder');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      // Set URL path cache context.
      '#cache' => ['contexts' => ['url.path']],
    ];

    // Configure API client from block settings.
    /** @var \Drupal\search_api\ServerInterface $server */
    $server = \Drupal::entityTypeManager()
      ->getStorage('search_api_server')
      ->load($this->configuration['server']);
    /** @var \Drupal\viko_ai\VikoAiClientInterface $client */
    $client = \Drupal::service('viko_ai.client')
      ->fromOptions($server->getBackendConfig()['client']);

    // Get folder ID based on URL first segment.
    $folder = viko_ai_get_folder();

    // Use default folder if empty.
    if (empty($folder) && !empty($this->configuration['default_folder'])) {
      $folder = $this->configuration['default_folder'];
    }

    if (!empty($folder)) {
      try {
        $result = $client->listFaqs($folder);
      }
      catch (\Exception $e) {
      }

      // Render FAQs provided by API.
      foreach ($result->faqs ?? [] as $faq) {
        $title = $faq->question;
        // Wrap title with link if available.
        if (!empty($faq->link)) {
          $title = '<a href="' . $faq->link . '">' . $title . '</a>';
        }
        $build[] = [
          // TODO: Replace by template.
          '#markup' => '<h3>' . $title . '</h3><p>' . $faq->answer . '</p>',
        ];
      }
    }

    return $build;
  }

}
