<?php

namespace Drupal\viko_ai;

use Drupal\Core\Http\ClientFactory;

/**
 * Viko.ai API client.
 */
class VikoAiClient implements VikoAiClientInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The HTTP client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * Constructs a new VikoAiClient instance.
   *
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   The HTTP client factory.
   */
  public function __construct(ClientFactory $http_client_factory) {
    $this->httpClientFactory = $http_client_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function fromOptions(array $config = []) {
    // Initialize API client.
    $this->httpClient = $this->httpClientFactory->fromOptions([
      'base_uri' => $config['base_uri'] . $config['tenancy'] . '/',
      'headers' => ['X-Api-Key' => $config['key']],
      'timeout' => 10,
    ]);
    return $this;
  }

  /**
   * Sends a Viko.ai API request.
   *
   * @param string $method
   *   The HTTP method.
   * @param string $uri
   *   The request URI.
   * @param int $status
   *   The response status.
   * @param array $headers
   *   The request headers.
   * @param string $body
   *   The request body.
   *
   * @return mixed
   *   The Viko.ai API call result.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  protected function request($method, $uri, $status = 200, $headers = [], $body = '') {
    try {
      // Encode body if JSON is provided.
      if (is_object($body)) {
        $body = json_encode($body);
      }
      // Prepare request and send it using API client.
      $response = $this->httpClient->request($method, $uri, [
        'headers' => $headers,
        'body' => $body,
      ]);
    }
    catch (\Exception $e) {
      throw new VikoAiClientException('Viko.ai API error: ' . $e->getMessage());
    }

    // Check if response status code is expected.
    if ($response->getStatusCode() === $status) {
      $type = $response->getHeader('Content-Type');
      $content = $response->getBody()->getContents();

      // Return decoded JSON data or plain text.
      if (strpos($type[0], 'application/json') !== FALSE) {
        return json_decode($content);
      }
      return $content;
    }
    throw new VikoAiClientException('Viko.ai API request status mismatch: '
      . $response->getStatusCode() . ' found, ' . $status . ' expected.');
  }

  /**
   * {@inheritdoc}
   */
  public function putDocument($folder, $id, $type, $body, $title = '', $link = '') {
    $uri = 'documents/' . $folder . '/' . $id;
    $headers = [
      'Content-Type' => $type,
      'X-Title' => $title,
      'X-Link' => $link,
    ];
    return $this->request('PUT', $uri, 202, $headers, $body);
  }

  /**
   * {@inheritdoc}
   */
  public function getDocument($folder, $id) {
    $uri = 'documents/' . $folder . '/' . $id;
    return $this->request('GET', $uri);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDocument($folder, $id) {
    $uri = 'documents/' . $folder . '/' . $id;
    return $this->request('DELETE', $uri, 202);
  }

  /**
   * {@inheritdoc}
   */
  public function listDocumentFolders() {
    $uri = 'documents';
    return $this->request('GET', $uri);
  }

  /**
   * {@inheritdoc}
   */
  public function listDocuments($folder) {
    $uri = 'documents/' . $folder;
    return $this->request('GET', $uri);
  }

  /**
   * {@inheritdoc}
   */
  public function putFaq($folder, $id, $question, $answer, $context = '', $link = '') {
    $uri = 'faqs/' . $folder . '/' . $id;
    $headers = [
      'Content-Type' => 'application/json',
    ];
    $body = (object) [
      'question' => $question,
      'answer' => $answer,
      'context' => $context,
      'link' => $link,
    ];
    return $this->request('PUT', $uri, 202, $headers, $body);
  }

  /**
   * {@inheritdoc}
   */
  public function getFaq($folder, $id) {
    $uri = 'faqs/' . $folder . '/' . $id;
    return $this->request('GET', $uri);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteFaq($folder, $id) {
    $uri = 'faqs/' . $folder . '/' . $id;
    return $this->request('DELETE', $uri, 202);
  }

  /**
   * {@inheritdoc}
   */
  public function listFaqFolders() {
    $uri = 'faqs';
    return $this->request('GET', $uri);
  }

  /**
   * {@inheritdoc}
   */
  public function listFaqs($folder) {
    $uri = 'faqs/' . $folder;
    return $this->request('GET', $uri);
  }

  /**
   * {@inheritdoc}
   */
  public function listAqs() {
    $uri = 'aqs';
    return $this->request('GET', $uri);
  }

  /**
   * {@inheritdoc}
   */
  public function askQuestion($text, $folders = []) {
    $uri = 'question';
    $headers = [
      'Content-Type' => 'application/json',
    ];
    $body = (object) [
      'text' => $text,
      'filter' => [
        'folders' => $folders,
      ],
    ];
    return $this->request('POST', $uri, 201, $headers, $body);
  }

  /**
   * {@inheritdoc}
   */
  public function answerQuestion($id, $accepted) {
    $uri = 'answer/' . $id;
    $headers = [
      'Content-Type' => 'application/json',
    ];
    $body = (object) [
      'accepted' => $accepted,
    ];
    return $this->request('PATCH', $uri, 200, $headers, $body);
  }

  /**
   * {@inheritdoc}
   */
  public function forwardQuestion($id, $email) {
    $uri = 'forward/' . $id;
    $headers = [
      'Content-Type' => 'application/json',
    ];
    $body = (object) [
      'reply_to' => $email,
    ];
    return $this->request('POST', $uri, 201, $headers, $body);
  }

  /**
   * {@inheritdoc}
   */
  public function search($text, $folders = []) {
    $uri = 'search';
    $headers = [
      'Content-Type' => 'application/json',
    ];
    $body = (object) [
      'text' => $text,
      'filter' => [
        'folders' => $folders,
      ],
    ];
    return $this->request('GET', $uri, 200, $headers, $body);
  }

  /**
   * {@inheritdoc}
   */
  public function reset() {
    $uri = 'reset';
    return $this->request('POST', $uri);
  }

}
