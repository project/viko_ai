<?php

namespace Drupal\viko_ai;

/**
 * Viko.ai API client interface.
 */
interface VikoAiClientInterface {

  /**
   * Configure a new client object.
   *
   * @param array $config
   *   The config for the client.
   *
   * @return $this
   *   The Viko.ai API client.
   */
  public function fromOptions(array $config = []);

  /**
   * Put document.
   *
   * @param string $folder
   *   The folder name.
   * @param string $id
   *   The document ID.
   * @param string $type
   *   The document type (text/plain | text/html | application/pdf).
   * @param string $body
   *   The document content.
   * @param string $title
   *   The document title (optional).
   * @param string $link
   *   The document URL (optional).
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function putDocument($folder, $id, $type, $body, $title = '', $link = '');

  /**
   * Get document.
   *
   * @param string $folder
   *   The folder name.
   * @param string $id
   *   The document ID.
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function getDocument($folder, $id);

  /**
   * Delete document.
   *
   * @param string $folder
   *   The folder name.
   * @param string $id
   *   The document ID.
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function deleteDocument($folder, $id);

  /**
   * List document folders.
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function listDocumentFolders();

  /**
   * List documents.
   *
   * @param string $folder
   *   The folder name.
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function listDocuments($folder);

  /**
   * Put FAQ.
   *
   * @param string $folder
   *   The folder name.
   * @param string $id
   *   The FAQ ID.
   * @param string $question
   *   The FAQ question.
   * @param string $answer
   *   The FAQ answer.
   * @param string $context
   *   The FAQ context (optional).
   * @param string $link
   *   The FAQ URL (optional).
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function putFaq($folder, $id, $question, $answer, $context = '', $link = '');

  /**
   * Get FAQ.
   *
   * @param string $folder
   *   The folder name.
   * @param string $id
   *   The FAQ ID.
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function getFaq($folder, $id);

  /**
   * Delete FAQ.
   *
   * @param string $folder
   *   The folder name.
   * @param string $id
   *   The FAQ ID.
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function deleteFaq($folder, $id);

  /**
   * List FAQ folders.
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function listFaqFolders();

  /**
   * List FAQs.
   *
   * @param string $folder
   *   The folder name.
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function listFaqs($folder);

  /**
   * List Answered Questions (AQs).
   *
   * @param string $folder
   *   The folder name.
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function listAqs();

  /**
   * Ask question.
   *
   * @param string $text
   *   The question text.
   * @param array $folders
   *   The question context (optional).
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function askQuestion($text, $folders = []);

  /**
   * Accept/reject answer.
   *
   * @param string $id
   *   The answer ID.
   * @param bool $accepted
   *   Set to TRUE if accepted, FALSE if rejected.
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function answerQuestion($id, $accepted);

  /**
   * Forward question.
   *
   * @param string $id
   *   The answer ID.
   * @param string $email
   *   The email to reply.
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function forwardQuestion($id, $email);

  /**
   * Search.
   *
   * @param string $text
   *   The searched text.
   * @param array $folders
   *   The search context (optional).
   *
   * @return object
   *   The API response.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function search($text, $folders = []);

  /**
   * Delete all documents & FAQs.
   *
   * @throws \Drupal\viko_ai\VikoAiClientException
   *   If the Viko.ai API error occurs.
   */
  public function reset();

}
