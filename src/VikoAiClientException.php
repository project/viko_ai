<?php

namespace Drupal\viko_ai;

/**
 * Viko.ai API exception.
 */
class VikoAiClientException extends \Exception {

}
